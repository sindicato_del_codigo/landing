window.onmousedown = function() {
	document.body.webkitRequestFullscreen();
};
window.onmousemove = function(event) {
	sideways = event.clientX / width * 10 - 5;
};
window.oncontextmenu = function() {
	colors = !colors;
	createLightning();
	return false;
};

var c = document.getElementById("c");
var ctx = c.getContext("2d");
var width = window.innerWidth,
	height = window.innerHeight;
var drops = [];
var increaseSpeed = 0.5;
var sideways = 0;
var colors;
Math.random() <= 0.001 ? colors = true : colors = false;
var extraPoints = [],
	extraTimer = 0,
	extraTime = 0.03;
var driplets = [];
var quoteGravityquote = 0.1;

function clear() {
	width = window.innerWidth;
	height = window.innerHeight;
	c.width = width;
	c.height = height;
}

function update() {
	for(var i in drops) {
		drops[i].fallSpeed += increaseSpeed;
		drops[i].y += drops[i].fallSpeed;
		drops[i].length = drops[i].fallSpeed * 2;
		if(drops[i].y >= height - 2) {
			driplets.push({
				x: drops[i].x,
				y: drops[i].y,
				mx: Math.random() - 0.5,
				my: -Math.random() * 2,
				time: 0
			});
			drops.splice(i, 1);
		}
		drops[i].x += sideways;
	}
	for(var i = 0; i < Math.floor(Math.random() * 15) + 5; i++) {
		colors ? rand = "rgb(" + Math.floor(Math.random() * 256) + ", " + Math.floor(Math.random() * 256) + ", " + Math.floor(Math.random() * 256) + ")" : rand = "#c0c0c0";
		drops.push({
			x: Math.random() * (width + 400) - 200,
			y: -Math.random() * 60,
			length: 0,
			fallSpeed: Math.random() * 5 + 5,
			color: rand
		});
	}
	if(Math.random() < 0.001) 
		createLightning();
	if(extraTimer >= extraTime) 
		extraPoints = [];
	else 
		extraTimer++;
	for(var i in driplets) {
		if(driplets[i].y >= height + 10) 
			driplets.splice(i, 1);
		driplets[i].x += driplets[i].mx + sideways / 3;
		driplets[i].y += driplets[i].my;
		driplets[i].my += quoteGravityquote;
	}
}
function draw() {
	requestAnimationFrame(draw);
	update();
	clear();
	ctx.globalAlpha = 0.3;
	ctx.lineWidth = 1;
	for(var i in drops) {
		ctx.strokeStyle = drops[i].color;
		ctx.beginPath();
		ctx.moveTo(drops[i].x, drops[i].y);
		ctx.lineTo(drops[i].x - sideways, drops[i].y - drops[i].length);
		ctx.stroke();
	}
	for(var i in driplets) {
		ctx.fillStyle = "#fff";
		ctx.beginPath();
		ctx.arc(driplets[i].x, driplets[i].y, 1, 0, 2 * Math.PI);
		ctx.fill();
	}
	ctx.globalAlpha = 1;
	ctx.lineWidth = 4;
	ctx.strokeStyle = "#ffffff";
	ctx.beginPath();
	ctx.moveTo(extraPoints[0].x, extraPoints[0].y);
	for(var i = 1; i < extraPoints.length; i++) 
		ctx.lineTo(extraPoints[i].x, extraPoints[i].y);
	ctx.stroke();
}
draw();